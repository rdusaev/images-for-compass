# Docker image for COMPASS

An image is based on upstream Alma9 distribution.
The main purpose of `Dockerfile` is to provide
COMPASS' [CORAL](https://gitlab.cern.ch/compass/coral)
and [Phast](https://gitlab.cern.ch/compass/phast) software for subsequent
usage in singularity containers. CORAL, Phast and Phast's
[UserEvents](https://gitlab.cern.ch/compass/phast.utils) get
installed in `/opt` dir.

## (Re)building Docker image

First, obtain relevant version of cernlib and COMPASS-specific software in pwd:

    $ git clone --depth 1ssh://git@gitlab.cern.ch:7999/CLHEP/CLHEP.git
    $ git clone --depth 1 -b development/rdusaev ssh://git@gitlab.cern.ch:7999/compass/coral.git
    $ git clone --depth 1 ssh://git@gitlab.cern.ch:7999/compass/phast.git
    $ git clone --depth 1 ssh://git@gitlab.cern.ch:7999/compass/phast.utils.git

or alternatively via https:

    $ git clone --depth 1 https://gitlab.cern.ch/DPHEP/cernlib/cernlib.git
    $ git clone --depth 1 -b development/rdusaev https://gitlab.cern.ch/compass/coral.git
    $ git clone --depth 1 https://gitlab.cern.ch/compass/phast.git
    $ git clone --depth 1 https://gitlab.cern.ch/compass/phast.utils.git

Then one can customize the `Dockerfile` if needed (by default it builds an
an alignment-specific flavour of CORAL). Follow comments in the Dockerfile for
guides.

    $ docker build -t gitlab-registry.cern.ch/rdusaev/images-for-compass .

will build the Docker image.

## Using image uploading to registry

Upload image to CERN gitlab registry with

    $ docker login gitlab-registry.cern.ch

Then push the image to project's registry:

    $ docker push gitlab-registry.cern.ch/rdusaev/images-for-compass

