# We're using Alma9 as default environment
#FROM almalinux:9
FROM centos:7

# Refresh packages index
RUN yum update -y
RUN yum install -y epel-release

#
# CERNLIB

# Install packages needed to build CERNLIB
RUN yum install -y make imake gfortran gcc gcc-gfortran \
    libXt-devel libX11-devel motif-devel libXaw-devel libnsl # NOTE: imake brought by EPEL
#? yum-plugins-core

# To mock cernlib build script
RUN ln -s /usr/lib64/libnsl.so.1 /usr/lib/libnsl.so
# ^^^ lib/lib64 is intentionally left there

# Copy cernlib sources, build and "install"
ENV CERN=/opt/cernlib
COPY cernlib /opt/cernlib.src
RUN cd /opt/cernlib.src \
 && ./make_cernlib

RUN yum install -y gcc-c++ \
    root root-geom root-genvector \
    mariadb-connector-c-devel xrootd-client-devel root-montecarlo-eg \
    expat-devel cmake gsl-devel

#
# CLHEP

# Install CLHEP (from source as there is no no rpm for Alma9)
ARG CLHEP_VER=2.2.0.6
RUN cd /tmp \
 && curl -O https://proj-clhep.web.cern.ch/proj-clhep/dist1/clhep-$CLHEP_VER.tgz \
 && tar xvvf clhep-$CLHEP_VER.tgz \
 && cd $CLHEP_VER \
 && mkdir build && cd build \
 && cmake ../CLHEP/ -DCMAKE_INSTALL_PREFIX=/usr/local/ \
 && make -j4 install \
 && rm /tmp/clhep*

RUN yum install -y curl-devel xrootd-client-devel mariadb-devel  # TODO

#
# CORAL

# 
#RUN ln -s /usr/lib64/libexpat.so /usr/lib/libexpat.so
# Copy and build CORAL
# NOTE: for branches other than development/rdusaev --with-CURL and --with-GSL
# are redundant
COPY coral /opt/coral
RUN cd /opt/coral \
 && ROOTSYS=/ DIR_CERN_LIBRARY=/opt/cernlib/new/ LDFLAGS=-L/usr/lib64/mysql ./configure \
 	--with-MySQL=/   	\
	--without-TGEANT        \
	--with-XROOTD=/usr      \
	--with-CERN_LIBRARY=/opt/cernlib/new \
	--with-CLHEP=/usr/local/ \
	--with-CFLAGS="-isystem /usr/include/xrootd -isystem /usr/include/mysql/ -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64" \
	--with-CURL=yes		\
	--with-GSL=yes 		\
	--with-EXPAT=/usr	\
 && (source ./setup.sh ; export CFLAGS="-I/usr/include/xrootd"; make)
# Build CORAL's utils, alignment-specific
RUN cd /opt/coral \
  ; CC=gcc source ./setup.sh \
  ; pushd src/alignment ; make -j4 ; popd \
  ; pushd src/track/trafdic/makeDico ; make -j4 ; popd \
  ; pushd src/condb/mysqldb/Utils ; make -j4 ; popd \
  ; pushd src/tests/realdata ; make -j4 ; popd

#
# Phast

#Copy and build Phast
COPY phast          /opt/phast
COPY phast.utils    /opt/phast.utils

RUN yum install -y root-tree-viewer

RUN cd /opt/coral \
 && CC=gcc source ./setup.sh \
 && cd /opt/phast \
 && sed -E 's/CERN_ROOT=[^[:space:]]+$/CERN_ROOT=\/opt\/cernlib\/new\//g' ./Makefile.lxplus_alma9 > Makefile \
 && cp ../phast.utils/user/UserJobEnd11.cc user/ -v \
 && cp ../phast.utils/user/UserEvent11*    user/ -v \
 && make -j4

RUN yum install -y git
RUN echo "cernlib $(cd /opt/cernlib.src ; git rev-parse --short HEAD)" >> /opt/compass-software-versions.txt \
  ; echo "clhep $CLHEP_VER" >> /opt/compass-software-versions.txt \
  ; echo "coral $(cd /opt/coral ; git rev-parse --short HEAD)" >> /opt/compass-software-versions.txt \
  ; echo "phast $(cd /opt/phast ; git rev-parse --short HEAD)" >> /opt/compass-software-versions.txt \
  ; echo "phast-utils $(cd /opt/phast.utils ; git rev-parse --short HEAD)" >> /opt/compass-software-versions.txt \
  ; rpm --query --all --queryformat '%{NAME} %{version}\n' >> /opt/compass-software-versions.txt


RUN echo -e "/usr/local/lib\n/usr/local/lib64" >> /etc/ld.so.conf.d/usr-local.conf
#
# Other (user) packages
RUN yum install -qy xrootd-client which ghostscript
